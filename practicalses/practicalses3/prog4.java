// WAP to find perfect number from an array and return index
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                 
		System.out.println("enter the array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("enter the array elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			int sum=0;
			for(int j=1;j<arr[i];j++){

				if(arr[i]%j==0){
					sum=sum+j;
				}
			}
			if(arr[i]==sum){
			   System.out.println(arr[i] +" "+ "is perfect num in array present at index"+" "+i);
			}
		}
	}
}
