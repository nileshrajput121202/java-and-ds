//Write a program to print the second max element in the array
//Input: Enter array elements: 2 255 2 1554 15 65
//Output: 255
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter the array size");
		int size= Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
                
	        int max=arr[0];
	        int smax=0;
		for(int i=0;i<arr.length;i++){
                           if(max<arr[i]){
				   smax=max;
				   max=arr[i];
			   } 
			   else if(max>arr[i] && smax<arr[i]){
				   smax=arr[i];
			   }
			   
		}
		 System.out.println("second maximum number :"+smax);        
		
	}
}
