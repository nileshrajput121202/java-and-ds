// WAP to find armstrong number from an array and return index
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                 
		System.out.println("enter the array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("enter the array elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			
			int cnt=0;
			int n=arr[i];
			int temp1=n;
			while(n!=0){
				cnt++;
				n=n/10;
			}
                        int sum=0;
			int temp2=temp1;
			while(temp1!=0){
				int rem=temp1%10;
				int prod=1;
				for(int j=1;j<=cnt;j++){
					prod=prod*rem;
				}
				sum=sum+prod;
				temp1=temp1/10;
			}
			
			if(temp2==sum){
			   System.out.println(arr[i] +" "+ "is armstrong num in array present at index"+" "+i);
			}
		}
	}
}
