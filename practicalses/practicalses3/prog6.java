// WAP to find strong number from an array and return index
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                 
		System.out.println("enter the array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("enter the array elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			int n=arr[i];
                       int sum=0;
			while(n!=0){

				int rem=n%10;
				int prod=1;
				for(int j=1;j<=rem;j++){
					prod=prod*j;
				}
				sum=sum+prod;
				n=n/10;
			}
			if(arr[i]==sum){
			   System.out.println(arr[i] +" "+ "is strong num in array present at index"+" "+i);
			}
		}
	}
}
