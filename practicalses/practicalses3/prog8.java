//Write a program to print count of digits in elements of array.
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter the array size");
		int size= Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			int n=arr[i];
                        int cnt=0;
			while(n!=0){
				cnt++;
				n=n/10;
			}
			System.out.print(" "+cnt);
		} 
		System.out.println();
	}
}
