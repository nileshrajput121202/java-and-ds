// WAP to reverse each elements in array
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                 
		System.out.println("enter the array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("enter the array elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			int n=arr[i];
			
			int rev=0;
			while(n!=0){
				int rem=n%10;
				rev=rev*10+rem;
				n=n/10;
			} 
			 
       		 System.out.println("array in reverse manner");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
