/*  
 *  1
 *  8 9
 *  27  16 125
 *  64 25 216 49
 */
class Demo{
	public static void main(String[] args){
		int row=4;
		int x=1;
		for(int i=1;i<=row;i++){
                        x=i;
			for(int j=1;j<=i;j++){
				
				if(j%2==0){
					System.out.print(x*x+" ");
				}
				else{
					 System.out.print(x*x*x+" ");
				}
		             x++;		

			}
			
			System.out.println();
		}
	}
}

