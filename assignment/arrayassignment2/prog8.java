/*   WAP to find the uncommon elements between two arrays.
 *   Input :
 *   Enter first array : 1 2 3 5
 *   Enter Second array: 2 1 9 8
 *   Output: Uncommon elements :
 *   3
 *   5
 *   9 */

import java.io.*;
class Demo8{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr1[]=new int[size];
		int arr2[]=new int[size];

		System.out.println("enter the array1 elements:");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());

		} 
                
		System.out.println("enter the array2 elements:");
		for(int j=0;j<arr2.length;j++){
			  arr2[j]=Integer.parseInt(br.readLine());       
		}	  

	        System.out.println("uncommon elements:"); 
		for(int i=0;i<size;i++){
                         int flag1=0;
			for(int j=0;j<size;j++){

				if(arr1[i]==arr2[j]){
                                   flag1++;					
				}
				    
		}
		            if(flag1==0)
				    System.out.print(arr1[i]+"\t");
              }  

	      for(int i=0;i<size;i++){
		      int flag2=0;
		      for(int j=0;j<size;j++){
			      if(arr2[i]==arr1[j]){
				      flag2++;
			      }
		      }
		      if(flag2==0)
			      System.out.print(arr2[i]+"\t ");
	      }
   }
} 


