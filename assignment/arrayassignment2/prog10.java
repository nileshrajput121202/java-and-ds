/*   WAP to print the elements whose addition of digits is even.
 *   Ex. 26 = 2 + 6 = 8 (8 is even so print 26)
 *   Input :
 *   Enter array : 1 2 3 5 15 16 14 28 17 29 123
 *   Output: 2 15 28 17 123*/

import java.io.*;
class Demo10{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter the array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

		} 

		System.out.println("elements whose addition of digits is even:");
	      
		for(int j=0;j<arr.length;j++){
			int num = arr[j];
			int rem;
			int sum =0;
			while(num!=0){

				rem=num%10;
				sum=sum+rem;
				num=num/10;
			}
			if(sum%2==0)
				System.out.println(arr[j]+" ");
		
	
		}
	System.out.println();	
	}
}


