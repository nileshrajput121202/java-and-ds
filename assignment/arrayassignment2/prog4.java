/*   WAP to search a specific element from an array and return its index. */

import java.io.*;
class Demo4{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter the array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

		} 

	        System.out.println("enter the element for search:");
		int ele=Integer.parseInt(br.readLine());
		for(int j=0;j<arr.length;j++){
                        
			if(ele==arr[j]){
				System.out.println("element found at index:"+j);
			}
			
		}
		
	}
}


