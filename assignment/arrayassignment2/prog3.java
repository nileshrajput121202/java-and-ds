/*   Write a Java program to find the sum of even and odd numbers in an array.  Display the sum value.*/

import java.io.*;
class Demo3{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter the array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

		} 

	        int sum1=0;
		int sum2=0;
		for(int j=0;j<arr.length;j++){

			if(arr[j]%2==0){
				sum1=sum1+arr[j];
			}
			else{
				sum2=sum2+arr[j];
			}
		}
		System.out.println("even sum is:"+sum1);
		System.out.println("odd sum is:"+sum2);
	}
}


