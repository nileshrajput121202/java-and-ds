/*   Write a program to create an array of ‘n’ integer elements.Where ‘n’ value should be taken from the user.
      Insert the values from users and find the sum of all elements in the array.*/

import java.io.*;
class Demo1{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter the array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

		} 

		int sum=0;
		for(int j=0;j<arr.length;j++){

			sum=sum+arr[j];
		}
		System.out.println("sum of array elements are:"+sum);
	}
}


