/*   WAP to find the common elements between two arrays.
    Input :
    Enter first array : 1 2 3 5
    Enter Second array: 2 1 9 8
    Output: Common elements :
    1
    2 */

import java.io.*;
class Demo7{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr1[]=new int[size];
		int arr2[]=new int[size];

		System.out.println("enter the array1 elements:");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());

		} 
                
		System.out.println("enter the array2 elements:");
		for(int j=0;j<arr2.length;j++){
			  arr2[j]=Integer.parseInt(br.readLine());       
		}	  

	        System.out.println("common elements are:");
		for(int i=0;i<size;i++){

			for(int j=0;j<size;j++){

				if(arr1[i]==arr2[j]){
					System.out.print(arr1[i]+"\t");
				}
		} 
	
		
	}
   }
} 


