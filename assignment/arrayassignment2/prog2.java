/*   WAP to find the number of even and odd integers in a given array of integers
 *   Input: 1 2 5 4 6 7 8
 *   Output:
 *   Number of Even Elements: 4
 *   Number of Odd Elements : 3*/

import java.io.*;
class Demo2{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter the array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

		} 

	        int cnt1=0;
		int cnt2=0;
		for(int j=0;j<arr.length;j++){

			if(arr[j]%2==0){
				cnt1++;
			}
			else{
				cnt2++;
			}
		}
		System.out.println("Number of even elements:"+cnt1);
		System.out.println("Number of odd elements:"+cnt2);
	}
}


