/*   WAP to take size of array from user and also take integer elements from user find the minimum element from the array */

import java.io.*;
class Demo5{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter the array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

		} 

	        int min = arr[0];
		for(int j=0;j<arr.length;j++){

			if(min>arr[j]){
				min=arr[j];
			}
		}
		System.out.println("minimum elements in array is :"+min);
		
	}
}


