//WAP to take size of array from user also take integer elements from user print multiplication of even elements only
import java.io.*;
class EDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter array size:");
                int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}
                int mul=1;
		for(int i=0;i<arr.length;i++){

			if(arr[i]%2==0){

				mul=mul*arr[i];
			} 
		} 
		System.out.println("multiplication  of even array:"+mul);
	}
}
