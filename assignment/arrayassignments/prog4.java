//WAP to take size of array from user also take integer elements from user print only vowels from the array
import java.io.*;
class RCB{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter array size:");
                int size=Integer.parseInt(br.readLine());

		char arr[]=new char[size];

		for(int i=0;i<arr.length;i++){

			arr[i]=(char)br.read();
			br.skip(1);
			

		 }
		System.out.println("vowels are:");
		for(int i=0;i<arr.length;i++){

			if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i'|| arr[i]=='o'|| arr[i]=='u'){
				System.out.print("  "+ arr[i]);
			}
		}
		System.out.println();
                
	}
}
