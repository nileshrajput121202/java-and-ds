//WAP to take size of array from user also take integer elements from user print product of odd index only
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter array size:");
                int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
                
		System.out.println("enter the array elements:");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}
                int prod=1;
		
		for(int i=0;i<arr.length;i++){

			if(i%2!=0){

				prod=prod*arr[i];
				
				
			} 
		} 
		System.out.println("product of odd index of  array:"+prod);
	}
}
