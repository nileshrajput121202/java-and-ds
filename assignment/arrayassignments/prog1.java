//WAP to take size of array from user also take integer elements from user print sum of odd elements
import java.io.*;
class ODemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter array size:");
                int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}
                int sum=0;
		for(int i=0;i<arr.length;i++){

			if(arr[i]%2!=0){

				sum=sum+arr[i];
			} 
		} 
		System.out.println("sum of odd array:"+sum);
	}
}
