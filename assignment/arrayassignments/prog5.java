//WAP to take size of array from user also take integer elements from user print only elements which is divisible by 5
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter array size:");
                int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
                
		System.out.println("enter the array elements:");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

		}
                

		for(int i=0;i<arr.length;i++){

			if(arr[i]%5==0){

				System.out.print("  "+arr[i]);
				
				
			} 
		} 
		System.out.println();
	}
}
