/*
 * class Demo{
 
      void fun(int x){  //fun(int )

      }
      void fun(int y){  //fun(int )

      }
      =>error
 }*/

/*
 * class Demo{
   
       int fun(int x){

       }
       flaot fun(int y){

       }
 }
 =>error return type no matter
 */


class Demo{
        void fun(int x){
           System.out.println(x);
	}

	void fun(float y){

		System.out.println(y);
	}

	void fun(Demo obj){
          System.out.println(obj);
	}

	public static void main(String[] args){
             Demo obj=new Demo();
	     obj.fun(10);
	     obj.fun(10.45f);
	     
	     Demo obj1=new Demo();
	     obj1.fun(obj);

	}

}
