class Demo{
	void fun(String str1){
		System.out.println("string");
	}

	void fun(StringBuffer str2){
		System.out.println("String Buffer");
	}
}
class Client{
	public static void main(String[] args){
		Demo obj=new Demo();
		obj.fun("Nilesh");
		obj.fun(new StringBuffer("Rajput"));
		//obj.fun(null);  error->ambigous
	}
}
