class Demo{
	int x=10;
	static int y=20;

	static{
		System.out.println("static block 1");

	}
}
class Client{

	static{
		Demo obj1=new Demo();
		System.out.println("static block 2");
		System.out.println(obj1.y);
	}

	public static void main(String[] args){

		Demo obj2=new Demo();
		System.out.println("main method");
		System.out.println(obj2.x);
	}
}
