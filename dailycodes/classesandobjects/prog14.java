class Demo{
	static 
	{
		System.out.println("In Demo static block");
	}
	public static void main(String[] args){
		System.out.println("In Demo main");
	}
}

class Client{
	static{
		System.out.println("In client static 1");
	}

	public static void main(String[] args){
		System.out.println("In client main");
	}

	static{
		System.out.println("in client static 2");
	}
}
