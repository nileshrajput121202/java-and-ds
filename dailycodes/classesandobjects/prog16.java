class Demo{
	static int x=10;//correct syntax

	static{
		static int y=20;//error
	}

	void fun(){
		static int z=30;//error
	}

	static void gun(){
		static int a=40;//error
	}

	public static void main(String[] args){

	}
}
