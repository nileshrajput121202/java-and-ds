class Demo{
	int x=10;
	Demo(){
		System.out.println("In No Args constructor");
		System.out.println(this.x);
	}

	Demo(int x){
		this();
		System.out.println("In para constructor");
		System.out.println(x);
	 } 

	public static void main(String[] args){
		
		Demo obj2=new Demo(20);
	}
}
