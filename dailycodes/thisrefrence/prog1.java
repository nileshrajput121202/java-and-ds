class Demo{
	int x=10;

	Demo(){
		System.out.println("In constructor");
		System.out.println(this.x);
		System.out.println(x);
		System.out.println(this);

	}
     
	void fun(){

		System.out.println(x);
		System.out.println(this.x);
		System.out.println(this);

	}
       

	public static void main(String[] args){
		Demo obj=new Demo();
		obj.fun();
		System.out.println(obj);
	}
}
