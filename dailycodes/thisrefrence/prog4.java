class Demo{
	int x=20;

	Demo(){
		System.out.println("In no arg constructor");//Demo(Demo this)
	}

	Demo(int x){
		System.out.println(x);
		System.out.println(this.x);
		System.out.println("In para constructor");//Demo(Demo this,int x)
	}

	public static void main(String[] args){

		Demo obj1=new Demo();//Demo(obj1)
		Demo obj2=new Demo(10);//Demo(obj2)
	}
}

//hidden this reference
