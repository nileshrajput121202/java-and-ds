import java.util.*;
class Demo{
   public static void main(String[] args){
     HashSet hs=new HashSet();
     hs.add("Nilesh");
     hs.add("Darshan");
     hs.add("Chetan");
     hs.add("Kunal");
     System.out.println(hs);
     
     System.out.println("*************************");
     HashMap hm=new HashMap();
     hm.put("Nilesh","Microsoft");
     hm.put("Darshan","Amazon");
     hm.put("Chetan","Netflix");
     hm.put("Kunal","Apple");
     System.out.println(hm);

   }
}
