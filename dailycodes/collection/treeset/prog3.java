import java.util.*;
class Movies implements Comparable{
      String mname=null;
      float coll=0.0f;
      Movies(String mname,float coll){
        this.mname=mname;
	this.coll=coll;
      }
       public String toString(){
         return mname+":"+coll;
      }
      public int compareTo(Object obj){
        return (mname.compareTo(((Movies)obj).mname));
      }
}
class TreeSetDemo{
      public static void main(String[] args){
             TreeSet ts=new TreeSet();
	     ts.add(new Movies("Gadar-2",125.05f));
	     ts.add(new Movies("Jailer",250.34f));
	     ts.add(new Movies("OMG-2",126.67f));
	     System.out.println(ts);    
    
          }
}
