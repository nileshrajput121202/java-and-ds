import java.util.*;
class MyClass implements Comparable<MyClass> {
          String name=null;
	  MyClass(String name){
            this.name=name;
	  }
         public String toString(){

             return name;
	 }
	 public int compareTo(MyClass obj){
             return (this.name).compareTo(obj.name);
	   // return (int)((name) - (obj.name));
	 }
}
class TreeSetDemo{
       public static void main(String[] args){
             TreeSet t=new TreeSet();
	     t.add(new MyClass("Nilesh"));
	     t.add(new MyClass("Sagar"));
	     t.add(new MyClass("Tom"));
	     t.add(new MyClass("Ben"));
	     t.add(new MyClass("Nilesh"));
	     System.out.println(t);
       }
}
