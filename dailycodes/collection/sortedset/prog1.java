import java.util.*;
class SortedSetDemo{
   public static void main(String[] args){
         SortedSet ss=new TreeSet();
	ss.add("Nilesh");
       ss.add("Darshan");
         ss.add("Chetan");  
          ss.add("Kunal");
       System.out.println(ss);	  
     
       System.out.println(ss.headSet("Nilesh"));
       System.out.println(ss.tailSet("Darshan"));
       System.out.println(ss.subSet("Chetan","Nilesh"));
       System.out.println(ss.first());
        System.out.println(ss.last());       
   }
}
