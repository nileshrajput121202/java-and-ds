import java.util.concurrent.*;
class Producer implements Runnable{
            BlockingQueue bq=null;
	    Producer(BlockingQueue bq){
                 this.bq=bq;
	    }
	    public void run(){
               for(int i=1;i<=5;i++){
                   try{
                     bq.put(i);
		   }catch(Exception obj){
		  
		   }
		   try{
                      Thread.sleep(1000);
		   }catch(Exception obj2){

		   }
	       }
	    }
}

class Consumer implements Runnable{
            BlockingQueue bq=null;
	    Consumer(BlockingQueue bq){
                 this.bq=bq;
	    }
	    public void run(){
               for(int i=1;i<=5;i++){
                   try{
                     bq.take();
		   }catch(Exception obj){
		  
		   }
		   try{
                      Thread.sleep(2000);
		   }catch(Exception obj2){

		   }
	       }
	    }
}
class Client{
        public static void main(String[] args){
             BlockingQueue bq=new ArrayBlockingQueue(3);

	     Producer produce=new Producer(bq);
	     Consumer consume=new Consumer(bq);

	     Thread pthread=new Thread(produce);
	     Thread cthread=new Thread(consume);

	     pthread.start();
	     cthread.start();
	}
}
