import java.io.*;
import java.util.*;
class PropertiesDemo{
        public static void main(String[] args)throws IOException{
        Properties obj=new Properties();
	FileInputStream inobj =new FileInputStream("friends.properties");
	obj.load(inobj);
	String name=obj.getProperty("Chetan");
	System.out.println(name);

	obj.setProperty("Pratik","Accenture");
	FileOutputStream outobj=new FileOutputStream("friends.properties");
	obj.store(outobj,"updated by Nilesh Rajput");

   }
}
