import java.util.*;
class Platform {
      String name=null;
      int foundyear=0;
      Platform(String name,int foundyear){
          this.name=name;
	  this.foundyear=foundyear;
      }
      public String toString(){
          return "{" + name + " : " + foundyear +"}";
      }
}
class SortByName implements Comparator{
      public int compare(Object obj1, Object obj2){
               return ((Platform)obj1).name.compareTo(((Platform)obj2).name);
      }
}
class TreeMapDemo{
       public static void main(String[] args){
      TreeMap tm=new TreeMap(new SortByName());
      tm.put(new Platform("You Tube",2005),"Google");
      tm.put(new Platform("Instagram",2010),"Meta");
      tm.put(new Platform("Facebook",2004),"Meta");
      tm.put(new Platform("ChatGPT",2022),"OpenAi");
     
      System.out.println(tm);
  }
}
