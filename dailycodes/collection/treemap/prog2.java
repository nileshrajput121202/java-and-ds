import java.util.*;
class Platform implements Comparable{
      String name=null;
      int foundyear=0;
      Platform(String name,int foundyear){
          this.name=name;
	  this.foundyear=foundyear;
      }
      public String toString(){
          return "{" + name + " : " + foundyear +"}";
      }

      public int compareTo(Object obj){
             return name.compareTo(((Platform)obj).name);
      }
}
class TreeMapDemo{
       public static void main(String[] args){
      TreeMap tm=new TreeMap();
      tm.put(new Platform("You Tube",2005),"Google");
      tm.put(new Platform("Instagram",2010),"Meta");
      tm.put(new Platform("Facebook",2004),"Meta");
      tm.put(new Platform("ChatGPT",2022),"OpenAi");
      System.out.println(tm);
  }
}
