import java.util.*;
class Demo{
     String str;
     Demo(String str){
      this.str=str;
     }
     public String toString(){
       return str;
     }
     public void finalize(){
        System.out.println("Notify...|");
     }
}
class Client{
       public static void main(String[] args){
          Demo obj1=new Demo("Core2Web");
	  Demo obj2=new Demo("Biencaps");
	  Demo obj3=new Demo("Incubator");

	  WeakHashMap wm= new WeakHashMap();
	  wm.put(obj1,2016);
	  wm.put(obj2,2017);
	  wm.put(obj3,2023);
	  System.out.println(wm);     
	  obj1=null;
	  System.gc();
	  System.out.println(wm);
       }
}
