import java.util.*;
class DequeDemo{
       public static void main(String[] args){
              Deque dq=new ArrayDeque();
	      dq.offer(20);
	      dq.offer(10);
	      dq.offer(40);
	      dq.offer(30);
	      dq.offer(50);
	      System.out.println(dq);

	      dq.offerFirst(100);
	      dq.offerLast(200);
	      System.out.println(dq);

	      dq.pollFirst();
	      dq.pollLast();
	      System.out.println(dq);

	      Iterator itr=dq.iterator();
	      while(itr.hasNext()){
                     System.out.println(itr.next());
	      }
	     Iterator itr2=dq.descendingIterator();
	      while(itr2.hasNext()){
                     System.out.println(itr2.next());
	      }

       }
}
