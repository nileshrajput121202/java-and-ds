import java.util.*;
class Employee{
      String ename=null;
      float sal=0;
      Employee(String ename,float sal){
       this.ename=ename;
       this.sal=sal;
      }
      public String toString(){
        return "{"+ ename + " : "+ sal +"}";
      }

}
class SortByName implements Comparator<Employee>{
       public int compare(Employee obj1,Employee obj2){
         return obj1.ename.compareTo(obj2.ename);
       }
}

class SortBySal implements Comparator <Employee>{
       public int compare(Employee obj1,Employee obj2){
          return -(int)(obj1.sal-obj2.sal);
       } 
}
class ArrayListDemo{
     public static void main(String[] args){
        ArrayList al=new ArrayList<Employee>();
	al.add(new Employee("Nilesh",150000.0f));
	al.add(new Employee("Darshan",140000.0f));
	al.add(new Employee("Chetan",130000.0f));
	al.add(new Employee("Kunal",120000.0f));
	System.out.println(al);

	Collections.sort(al,new SortByName());
	System.out.println(al);

	Collections.sort(al,new SortBySal());
	System.out.println(al);
     }
}
