import java.util.*;
class IndianPms{
      String pmname=null;
      int termofoffice=0;
      IndianPms(String pmname,int termofoffice){
       this.pmname=pmname;
       this.termofoffice=termofoffice;
      }
      public String toString(){
        return "{"+ pmname + " : "+ termofoffice +"}";
      }

}
class SortByName implements Comparator<IndianPms>{
       public int compare(IndianPms obj1,IndianPms obj2){
         return obj1.pmname.compareTo(obj2.pmname);
       }
}

class SortByTermOfOffice implements Comparator <IndianPms>{
       public int compare(IndianPms obj1,IndianPms obj2){
          return -(int)(obj1.termofoffice-obj2.termofoffice);
       } 
}
class ArrayListDemo{
     public static void main(String[] args){
        ArrayList al=new ArrayList<IndianPms>();
	al.add(new IndianPms("Pandit Nehru",16));
	al.add(new IndianPms("Indira Gandhi",11));
	al.add(new IndianPms("Rajiv Gandhi",5));
	al.add(new IndianPms("Manmohan Sing",10));
	al.add(new IndianPms("Narendra Modi",10));
	System.out.println(al);

	Collections.sort(al,new SortByName());
	System.out.println(al);

	Collections.sort(al,new SortByTermOfOffice());
	System.out.println(al);
     }
}
