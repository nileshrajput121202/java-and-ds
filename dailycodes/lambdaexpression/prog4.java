import java.util.*;
class Employee{
       String name=null;
       int id=0;
       Employee(int id,String name){
        this.name=name;
	this.id=id;
       }
       public String toString(){
            return name+" : "+id;
       }

}
class Demo{
          public static void main(String[] args){
             ArrayList obj=new ArrayList();
	     obj.add(new Employee(20,"Nilesh"));
	     obj.add(new Employee(21,"Darshan"));
	     obj.add(new Employee(22,"Chetan"));
	     obj.add(new Employee(23,"Balaji"));
	     System.out.println(obj);

	     Collections.sort(obj,(obj1,obj2)->{
	           return ((Employee)obj1).name.compareTo(((Employee)obj2).name	);	     
	     }  );
	     System.out.println(obj);
	  }
}
