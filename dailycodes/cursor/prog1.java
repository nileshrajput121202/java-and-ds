//ITERATOR`			
import java.util.*;									
class Demo{
   public static void main(String[] args){
       ArrayList al=new ArrayList();
       al.add("Nilesh");
       al.add("Vishal");
       al.add("Darshan");
       al.add("Pratik");
       System.out.println(al);

       Iterator itr=al.iterator();
       while(itr.hasNext()){
            if("Darshan".equals(itr.next()))
		    itr.remove();
       }
        System.out.println(al);       
   }
}
