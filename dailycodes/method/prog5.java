class Demo{
	void fun(int x){
		System.out.println(x);
	}

	public static void main(String[] args){
		System.out.println("in main");
		Demo obj = new Demo();
		obj.fun(10);
		obj.fun(23.55f);
		System.out.println("end main");
	}
}

//error = incompatible type
