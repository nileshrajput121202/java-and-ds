// class stringtokenizer
import java.io.*;
import java.util.*;
class StringDemo{

	public static void main(String[] args) throws IOException{

	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	System.out.println("enter player name,runs,grade,average");
	String info = br.readLine();
        System.out.println(info);

	StringTokenizer st = new StringTokenizer(info, ",");

	String t1 = st.nextToken();
	int t2 = Integer.parseInt(st.nextToken());
	String t3 = st.nextToken();
	float t4 = Float.parseFloat(st.nextToken());

	System.out.println("player name = "+t1);
	System.out.println("runs="+t2);
	System.out.println("grade="+t3.charAt(0));
	System.out.println("average="+t4);

	}
}
