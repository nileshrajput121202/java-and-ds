class Demo{
	public static void main(String[] args){
		String str1 = "Nilesh";
		String str2 = new String("Rajput");
		StringBuffer str3 = new StringBuffer("computer");

	     //	String str4 = str3.append(str1); error-incampatible type
	     //	String str5 = str1.append(str3); error-cannot find symbol
	        
		StringBuffer str6 = str3.append(str1);

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str6);
	}
}
