class Demo{
	public static void main(String[] args){

		String str1 = "Nilesh";
		String str2 = "Nilesh";

		String str3 = new String("Rajput");
		String str4 = new String("Rajput");

		System.out.println(System.identityHashCode(str1));
	         System.out.println(System.identityHashCode(str2));

                 System.out.println(System.identityHashCode(str3));
      		 System.out.println(System.identityHashCode(str4));         
	}
}
// identity hasg code of str1 and str2 are same bcz both are have same object in string constant pool
// str3 and str4 has different identity hashcode bcz both are made new object
