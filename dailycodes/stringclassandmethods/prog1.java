class Demo{
	public static void main(String[] args){

		String str1 = "Nilesh";
		String str2= new String("Rajput");
		char str3[]={'s','u','n'};

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);

	}
 }

// in java string declaration types.
// str1 goes on string constant pool
// str2 make new object of string on heap

