class Demo{
	public static void main(String[] args){

		String str1 ="Nilesh";
		String str2 = str1;
		String str3 = new String(str2);

		System.out.println(System.identityHashCode(str1));
		 System.out.println(System.identityHashCode(str2));
                 System.out.println(System.identityHashCode(str3)); 
	}
}

// str1 and str2 having same identityhashcode bcz same object on string constatnt pool
//
//for str3 new object is creat so having different identity hash code


