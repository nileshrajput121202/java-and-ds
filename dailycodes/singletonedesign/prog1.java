class SingleTone{
	static SingleTone obj=new SingleTone();
	private SingleTone(){
		System.out.println("constructor");
	}
	static SingleTone getdata(){
		return obj;
	}
}
class Client{
	public static void main(String[] args){
		SingleTone obj1=SingleTone.getdata();
		System.out.println(obj1);

		SingleTone obj2=SingleTone.getdata();                                                                                   System.out.println(obj2);

	       SingleTone obj3=SingleTone.getdata();                                                                                   System.out.println(obj3);    
	}
}	
