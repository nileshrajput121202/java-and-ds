class MyThread extends Thread{
       MyThread(ThreadGroup tg,String str){
           super(tg,str);
       }
       public void run(){
            System.out.println(Thread.currentThread());
       }
}
class ThreadDemo{
      public static void main(String[] args){
          ThreadGroup obj=new  ThreadGroup ("Nilesh");
	  MyThread obj1=new MyThread(obj,"x");
	  MyThread obj2=new MyThread(obj,"y");   
	  obj1.start();
	  obj2.start();
      }
}
