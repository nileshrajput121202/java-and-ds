import java.util.*;
class BankLimitException extends RuntimeException{
           BankLimitException(String str){
                 super(str);
	   }

}
class Demo{
       public static void main(String[] args){
           Scanner sc=new Scanner(System.in);
	   System.out.println("enter amount");
	   float val=sc.nextFloat();
	   if(val>10000){
                throw new BankLimitException("Limit Cross");

	   }
	   System.out.println("Money Withdraw");
       }

}
