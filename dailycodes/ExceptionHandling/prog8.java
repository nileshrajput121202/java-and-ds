import java.util.*;
class DataOverflow extends RuntimeException{
           DataOverflow(String msg){
                 super(msg);
	   }
}

class DataUnderflow extends RuntimeException{

           DataUnderflow(String msg){

                super(msg);
	   }
}

class Client{
          public static void main(String[] args){
              int arr[]=new int[5];
	      Scanner sc=new Scanner(System.in);
	      for(int i=0;i<arr.length;i++){
                  int val=sc.nextInt();

		  if(val<0){

                       throw new DataUnderflow("data is less than 0");
		  }

		  if(val>100){
                      throw new DataOverflow("Data greater than 100");
		  }

		  arr[i]=val;

	      }

	      for(int i=0;i<arr.length;i++){

                  System.out.println(arr[i]);
	      }
	  }

}
