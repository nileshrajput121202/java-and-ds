interface Demo{
	void gun();
	default void fun(){
               System.out.println("in fun Demo");      
	}
}
class Child implements Demo{
       public void gun(){
           System.out.println("in child gun");      

       }
}

class Client{

         public static void main(String[] args){
           Demo obj=new Child();
	   obj.gun();
	   obj.fun();
	 }

}
