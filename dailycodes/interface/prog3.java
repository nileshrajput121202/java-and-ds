interface Demo1{
        void fun();
}

interface Demo2{
        void fun();
}

class Demo3 implements Demo1,Demo2{
    public void fun(){
        System.out.println("in fun fun fun ):");      

    }
}

class Client{
     public static void main(String[] args){
          Demo3 obj = new Demo3();
	  obj.fun();
     }
}
