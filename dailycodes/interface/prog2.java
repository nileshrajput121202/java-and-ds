//it ca not consist of constructor
//100 % abstraction
//object can not be made
//methods in interface by default public abstract
//.class file banate

interface Demo{
          void fun();
	  void gun();
}
abstract class DemoChild1 implements Demo{
	
	public void fun(){
             System.out.println("in demochild fun");
	}


 }

class DemoChild2 extends DemoChild1{
         public void gun(){
              System.out.println("in demochild2 gun");      
	 }

}


class Client{
       public static void main(String[] args){

       Demo obj=new DemoChild2();
       obj.fun();
       obj.gun();
       }
}
