interface Demo{

	int x=10;//in interface variable are public static final 
	void fun();
}
class Child implements Demo{
         
	 public void fun(){

          System.out.println(x);
	  System.out.println(Demo.x);

	 }
}
class Client{
  public static void main(String[] args){
        
	  Child obj=new Child();
	  obj.fun();
  }
}
