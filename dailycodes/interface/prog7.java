interface Demo{
	static void fun(){
          System.out.println("in static fun");      

	}
}
class Child implements Demo{

}

class Client{
	public static void main(String[] args){
           // Demo obj=new Child();
	   // obj.fun();  gives error static void fun cant implements in child class so we can call that method by the name of interface
	    
		Demo.fun();
 
	}
}
