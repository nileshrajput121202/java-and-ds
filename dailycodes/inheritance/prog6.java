class Parent{
	int x=10;
	static int y=20;
	static{
		  System.out.println("parent static block");
	}

      Parent(){
          System.out.println("in constructor");      
      }	      

      void method1(){

	        System.out.println(x);      
		  System.out.println(y);
      }
  
   static void method2(){
       System.out.println(y); 
   }
}

class Child extends Parent{
              static{
                   System.out.println("in child static block");      
	      }
	      Child(){
                  System.out.println("in child constructor");      
	      }
}

class Client{
          public static void main(String[] args){

                   Child obj=new Child();
		   obj.method1();
		   obj.method2();

	  }
	}
