class Parent{
	Parent(){
		System.out.println("In parent class Constructor");
	}

	void pproperty(){
                System.out.println("flat-gold-money");
	}
}

class Child extends Parent{

	Child(){
           System.out.println("In child class constructor");
	}

}

class Client{
          public static void main(String[] args){
          Child obj=new Child();
	  obj.pproperty();
}

}
