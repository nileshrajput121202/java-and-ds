class Player{
	private int jerno=0;
	private String name=null;

	Player(int jerno,String name){
                this.jerno=jerno;
		this.name=name;
		System.out.println("In player constructor");

	}
	void pinfo(){

		System.out.println(jerno + "=" + name);
	}
}
class Client{

	public static void main(String[] args){

		Player obj1=new Player(45,"Ro-hit");
		obj1.pinfo();

		Player obj2=new Player(7,"MSD");
		obj2.pinfo();

	}
}
