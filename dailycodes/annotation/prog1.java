/* three types 
 *  1)override
 *  2)depricated
 *  3)suppresswarning */

class Parent{
    void m1(){
         System.out.println("in parent m1");
    }
}
class Child extends Parent{
  @Override	
   void m1(int x){
         System.out.println("in child m1");     
   }
}
class Client{
        public static void main(String[] args){
               Parent obj= new Child();
	       obj.m1();
	}
}
