abstract class Defence{
	void moto(){
		System.out.println("protect india from danger");
	}
	abstract void uniform();
}
class Army extends Defence{
	void uniform(){
		  System.out.println("olive green");
	}
}
class Neavy extends Defence{
	void uniform(){
		  System.out.println("white");
	}
}
class AirForce extends Defence{
	void uniform(){
		  System.out.println("sky blue");
	}
}	

class Client{
        public static void main(String[] args){
	AirForce obj1 = new AirForce();
	obj1.uniform();

	Neavy obj2=new Neavy();
	obj2.uniform();

	Army obj3 = new Army();
	obj3.uniform();
}
}
