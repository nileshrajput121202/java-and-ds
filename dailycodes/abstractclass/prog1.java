//when parent class know that child class override parent method at that time abstrct class is use
//constructor is present
//.class file is made
//0-100 perc abstraction
//abstract class cannot make its object
abstract class Parent{
	void career(){
           System.out.println("doctor");
	}
	abstract void marry();

}
class Child extends Parent{
	void marry(){
		System.out.println("kirthi shetty");
		}
}

class Client{
	public static void main(String[] args){
		Parent obj1=new Child();
		obj1.marry();
		obj1.career();
	}

}
