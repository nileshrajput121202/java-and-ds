// if and if else 
// temp in fernheit ,print whether person has high,normal,low temp;
//  >98.6....high
//  98.0<= and <=98.6...normal
//  <98.0 ...normal
class Demo {
	public static void main(String[] args){
		float temp=99;
		
		if(temp>98.6f){
			System.out.println("high");
		} 
		else if(temp<98.0f){
			System.out.println("low");  
		}		
		else{
			System.out.println("normal");   
		}
	}
}


