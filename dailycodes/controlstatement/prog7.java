// if and if else 
// given integer value
// print fizz if divisible by 3
// print buzz if divisible by 5
// print fizz buzz divisible by both
// if not print not divisible by by both
class Demo {
	public static void main(String[] args){
		int x=5;
		
		if(x%3==0 && x%5==0){
			System.out.println("fizz buzz");
		} 
		else if(x%3==0){
			System.out.println("fizz ");
		}
	       else if(x%5==0){
	            System.out.println("buzz");   
	       }		    
		 
		else{
			System.out.println("not divisible by both");   
		}
	}
}


