//loop
//take an integer N as input print sum of its digit

class Demo{
	public static void main(String[] args){
		int n=567;
		int i=1;
		int sum=0;
		while(i<=n){
			int rem=n%10;
			sum=sum+rem;
			n=n/10;
		}
		System.out.println("sum= "+sum);
	}
}
