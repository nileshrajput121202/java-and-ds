// Define the Days enum
enum Days {
    monday,
    tuesday,
    wednesday,
    sunday // You've defined Sunday, but it's not printed in the main method
}

// Define the Demo class
class Demo {
    public static void main(String[] args) {
        // Print the values of the Days enum
        System.out.println(Days.monday);     // Prints "monday"
        System.out.println(Days.tuesday);    // Prints "tuesday"
        System.out.println(Days.wednesday);  // Prints "wednesday"
        
	Days obj= Days.wednesday;
	System.out.println(obj.ordinal());
    }
}
