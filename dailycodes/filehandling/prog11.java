import java.io.*;
class Leader implements Serializable{
     int age=0;
     String name=null;
     Leader(int age,String name){
          this.age=age;
	  this.name=name;
     }
}
class SerializableDemo{
         public static void main(String[] args)throws IOException{
            Leader obj1=new Leader(72,"NarendraModi");
	    Leader obj2=new Leader(54,"RahulGandhi");

	    FileOutputStream fos=new FileOutputStream("leader1.txt");
            ObjectOutputStream oo=new ObjectOutputStream(fos);
	    oo.writeObject(obj1);
	    oo.writeObject(obj2);

	    oo.close();
	    fos.close();
	 }
}
