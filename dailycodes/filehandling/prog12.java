import java.io.*;
class Leader implements Serializable{
     int age=0;
     String name=null;
     Leader(int age,String name){
          this.age=age;
	  this.name=name;
     }
}
class DeserializableDemo{
         public static void main(String[] args)throws IOException,ClassNotFoundException{

	    FileInputStream fis=new FileInputStream("leader1.txt");
            ObjectInputStream oo=new ObjectInputStream(fis);
            
	    Leader obj1=(Leader)oo.readObject();           
	    Leader obj2=(Leader)oo.readObject();

	    oo.close();
	    fis.close();

	    System.out.println(obj1.age +" "+obj1.name);
	    System.out.println(obj2.age+" "+obj2.name);
	 }
}
