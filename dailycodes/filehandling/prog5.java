import java.io.*;
class Demo{
        public static void main(String[] args)throws IOException{
            File ob=new File("friend.txt");
	    ob.createNewFile();

	    System.out.println(ob.getName());
	    System.out.println(ob.getParent());
	    System.out.println(ob.getPath());
	    System.out.println(ob.getAbsolutePath());
	    System.out.println(ob.canRead());
	    System.out.println(ob.canWrite());
	    System.out.println(ob.isDirectory());
	    System.out.println(ob.isFile());
	}
}
