import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
          
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr[][]=new int [2][3];
                
		System.out.println("enter array elements:");
		for(int i=0;i<2;i++){
			
			for(int j=0;j<3;j++){

				arr[i][j]=Integer.parseInt(br.readLine());

			}
		} 

		System.out.println("2-D array elements are:");

		for(int i=0;i<2;i++){

			for(int j=0;j<3;j++){

				System.out.print(arr[i][j]+" ");

			}
			System.out.println();
		}
	}
}

