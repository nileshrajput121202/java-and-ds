class outer{
      void m1(){
          System.out.println("in m1 outer");
      }

      class inner{
          void m2(){
             System.out.println("in m2 inner");    
	  }
      }

         public static void main(String[] args){

              outer obj1=new outer();
	      obj1.m1();

	      inner obj2=new outer().new inner();
	      obj2.m2();
	 }
}
