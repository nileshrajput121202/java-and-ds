class Parent{
          Parent(){
             System.out.println("parent constructor");
	  }

	  void fun(int x){
               System.out.println("parent fun");
	  }
}
class Child extends Parent{
          Child(){
            System.out.println("child constructor");
	  }

	  void fun(){
            System.out.println("child fun");
	  }

}

class Client{
         public static void main(String[] args){
           Parent obj=new Child();
	   obj.fun();
	 }
}//error in code
