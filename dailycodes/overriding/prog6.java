class Parent{
	char fun(){
         	System.out.println("parent fun");
		return 'a';
	}
}
class Child extends Parent{
	char fun(){
		System.out.println("child fun");
		return 'b';
	}
}
class Client{
	public static void main(String[] args){

		Parent obj =new Child();
		obj.fun();
	}
} 
//for primitive parent child must have same return type
