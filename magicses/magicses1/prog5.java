// program to print the square of even digits of given number
class Demo{
	public static void main(String[] args){
		int n=23456;
		int i=1;
		while(n!=0){
			int rem=n%10;
			if(rem%2==0){
				System.out.println(rem*rem);
			}
			n=n/10;
		}
	}
}
