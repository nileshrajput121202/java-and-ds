/* print all even numbers in reverse order and odd numbers in std way.both separetly within same range take start and end from user*/
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter starting value ");
		int start = Integer.parseInt(br.readLine());
		System.out.println("enter ending value");
		int end = Integer.parseInt(br.readLine());

		for(int i=end;i>=start;i--){
			if(i%2==0){
				System.out.print(" "+i);
			}
		} 
		System.out.println();

		for(int i=start;i<=end;i++){
			if(i%2!=0){
				System.out.print(" "+i);
			}
		}
		System.out.println();
	}
}
