/* write a program and take two character if these character equal then print them as it is but if they are unequal print their difference*/
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter character 1");
		char ch1 = (char)br.read();
		br.skip(1);
		System.out.println("enter character 2");
		char ch2 =(char)br.read();

		if(ch1==ch2){
			System.out.println(ch1 + "  "+ ch2);
		}
		else{
			int cnt=0;
			char temp=ch1;
			while(true){
				if(ch1==ch2){
					break;
				}
				cnt++;
				ch1++;
			}
			System.out.println("Difference between "+ temp + " and "+ch2+  " is "+cnt);
		}
	}
}

