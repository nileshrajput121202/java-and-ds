/*  0
 *  1 1
 *  2 3 5
 *  8 13 21 24
 */ 
import java.io.*;
class  FDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the row count");
		int row= Integer.parseInt(br.readLine());

		int n1=0;
		int n2=1;
		int n3=0;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
                                
				System.out.print(n3 + "  ");
				n1=n2;
				n2=n3;
				n3=n1+n2;
			
			}
			System.out.println();
		}
	}
}
