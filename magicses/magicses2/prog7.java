/* O
 * 14 13
 * L K J
 * 9 8 7 6
 * E D C B A
 */
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the rows");
		int row = Integer.parseInt(br.readLine());;;
		char ch = 'A';

		int num = row * (row+1)/2;
		ch += num-1;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(i%2==0){
					System.out.print(num +"  ");
					
				}
				else{
					System.out.print(ch+"  ");
					
				}
				num--;
				ch--;
			}
			System.out.println();
		}
	}
}
