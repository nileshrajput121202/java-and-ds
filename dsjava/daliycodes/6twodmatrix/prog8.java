
//rotate matrix by 90 degree in clockwise direction
class Demo{
    public static void main(String[] args){
          int arr[][]=new int[][]{{1,2,3},{4,5,6},{7,8,9}};
	  
	  for(int i=0;i<arr.length;i++){
              for(int j=0;j<arr.length;j++){
                  System.out.print(arr[i][j]+" ");
	      }
	      System.out.println();
	  }
	  for(int i=0;i<arr.length;i++){
              for(int j=i+1;j<arr.length;j++){
                  int temp=arr[i][j];
		  arr[i][j]=arr[j][i];
		  arr[j][i]=temp;
	      }
	  }
	  System.out.println();

	  for(int i=0;i<arr.length;i++){
               int s=0;
	       int e=arr.length-1;
	       while(s<e){
                  int temp=arr[i][s];
		  arr[i][s]=arr[i][e];
		  arr[i][e]=temp;
		  s++;
		  e--;
	       }
	  } 
	   System.out.println();           
	  for(int i=0;i<arr.length;i++){
              for(int j=0;j<arr.length;j++){
                  System.out.print(arr[i][j]+" ");
	      }
	      System.out.println();
	  }
    }
}
