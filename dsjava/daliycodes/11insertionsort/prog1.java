import java.util.*;
class InsertionSort{
	 static void isort(int arr[]){
             for(int i=1;i<arr.length;i++){
                 int ele=arr[i];
		 int j=i-1;

		 while(j>=0 && arr[j]>ele){
                      arr[j+1]=arr[j];
		      j--;
		 }
		 arr[j+1]=ele;
	     }
	 }
        public static void main(String[] args){
          int arr[]=new int[]{2,3,8,4,3,6,9};
	  System.out.println(Arrays.toString(arr));
	  isort(arr);
	  System.out.println(Arrays.toString(arr));            
	}
}
