// sec largest ele in array
class Demo{
      public static void main(String []args){
          int arr[]=new int[]{6,3,2,7,8,4,3};
	  int max=Integer.MIN_VALUE;
	  int sec_large=0;

	  for(int i=0;i<arr.length;i++){
		 if(arr[i]>max) {
               sec_large=max;
	       max=arr[i];
	  }
	  }
	  System.out.println("Max Element:"+max);
	  System.out.println("Sec Max Element:"+sec_large);
      }
}
