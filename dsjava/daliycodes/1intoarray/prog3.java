/*
   given an integr array size N
   count the no of elements having atleast 1 element greater than itself
  */
//optimize code
class Demo{
       public static void main(String[] args){
            int arr[]=new int[]{2,5,1,4,8,0,8,1,3,8};
	    int n=10;
	    int max=Integer.MIN_VALUE;
	    for(int i=0;i<n;i++){
                if(arr[i]>max){
                  max=arr[i];
		}
	    }
	    int cnt=0;
	    for(int i=0;i<n;i++){
                  if(arr[i]==max){
                     cnt++;
		  }
	    }
	    System.out.println(n-cnt);
       }
}
