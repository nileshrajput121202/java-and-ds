import java.util.*;
class Demo {
      static int fun(int arr[],int search){
          for(int i=0;i<arr.length;i++){
                 if(search==arr[i]){
                     return i;
		 }
	  }
	  return -1;
      }
      public static void main(String[] args){
         Scanner sc=new Scanner(System.in);
	 System.out.println("Enter Array Size");
	 int size=sc.nextInt();
	 System.out.println("Enter Array Elements");
	 int arr[]=new int[size];

	 for(int i=0;i<arr.length;i++){
           arr[i]=sc.nextInt();
	 }
	 System.out.println();

	 System.out.println("array elements are:");
	 
	 for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+ " ");
	 }

	 System.out.println("Enter Element for Search");
	 int search=sc.nextInt();
	  int ret=fun(arr,search);   
          if(ret==-1){
               System.out.println("enter proper array element for search");
	  }else{
	 
	 System.out.println(ret);
	  }
      }
}
