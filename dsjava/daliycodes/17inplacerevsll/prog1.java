import java.util.*;
class Node{
       int data;
       Node next=null;
       Node(int data){
         this.data=data;
       }
}
class SLinkedList{
      Node head=null;
      void addnode(int data){
         Node newnode = new Node(data);
	 if(head==null){
             head=newnode;
	 }else{
             Node temp=head;
	     while(temp.next!=null){
                temp=temp.next;
	     }
	     temp.next=newnode;
	 }
      }
      void print(){
         Node temp=head;
	 while(temp.next!=null){
            System.out.print(temp.data +"->");
            temp=temp.next;
	 }
	 System.out.println(temp.data);
        }

      int countnode(){
          Node temp=head;
	  int cnt=0;
	  while(temp!=null){
             cnt++;
	     temp=temp.next;
	  }
	  System.out.println("count="+cnt);
	  return cnt;
      }

      void revitr(){
         Node prev=null;
	 Node curr=head;
	 Node forw=null;

	 while(curr!=null){
           forw=curr.next;
	   curr.next=prev;
	   prev=curr;
	   curr=forw;
	 }
	 head=prev;
      }

      void revrec(Node prev,Node curr){
            if(curr==null){
               head=prev;
	       return;
	    }
	    else{

                Node forw=curr.next;                                                                                                       curr.next=prev;                                                                                                       prev=curr;                                                                                                            curr=forw;      
	    }
	    revrec(prev,curr);
      }

      void middle(){
         int len=countnode();
	 int cnt=0;
	 Node temp=head;
	 while(cnt<len/2){
             temp=temp.next;
	     cnt++;
	 }
	 System.out.println("Middle of LL : "+temp.data);
      }

      void middleptr(){
         Node slow=head;
	 Node fast=head.next;
	 while(fast!=null){
              fast=fast.next;
	      if(fast!=null)
		       fast=fast.next;    
	      slow=slow.next;
	 }
	    System.out.println("Middle of LL : "+slow.data);        
      }

}
class Client{
       public static void main(String[] args){
         Scanner sc=new Scanner(System.in);
	 char ch;
	   SLinkedList ll=new SLinkedList();   
	 do{
            System.out.println("1.addnode");                                                                                      System.out.println("2.print");                                                                                        System.out.println("3.countnode"); 
            System.out.println("4.revitr");
	    System.out.println("5.revrec");
	    System.out.println("6.middle");
	    System.out.println("enter choice");
	    int choice=sc.nextInt();
	    
	    switch(choice){
               case 1:
		       {
                          System.out.println("enter data");
			  int data=sc.nextInt();
			  ll.addnode(data);
		       }
		       break;
	       case 2:
		       {
                         ll.print();
		       }
		       break;
	       case 3:
		       {
                         ll.countnode();
		       }
		       break;
	       case 4:
		       {

                        ll.revitr();
		       }
		       break;
	       case 5:
		       {
			Node prev=null;
                        ll.revrec(prev,ll.head);
		       }
		       break;
	       case 6:
		       {
                         ll.middle();
		       }
		       break;
	       default:
		       System.out.println("wrong input");
	    }
	    System.out.println("do you want to continue..?");
	    ch=sc.next().charAt(0);

	 }while(ch=='y' || ch=='Y');
       }
}
