import java.util.*;
class Quick{
	static void qsort(int arr[],int start,int end){
             if(start<end){
               int pivot=partition(arr,start,end);
	       qsort(arr,start,pivot-1);
	       qsort(arr,pivot+1,end);
	     }
	}
	static int partition(int arr[],int start,int end){
              int pivot=arr[end];
	      int i=start-1;

	      for(int j=start;j<end;j++){
                   if(arr[j]<=pivot){
                      i++;
		      int temp=arr[i];
		      arr[i]=arr[j];
		      arr[j]=temp;
		   }
	      }
	      int temp=arr[i+1];
	      arr[i+1]=arr[end];
	      arr[end]=temp;
	      return i+1;
	}

       public static void main(String[] args){
          int arr[]=new int[]{6,8,3,5,9,15};
	  int start=0;
	  int end=arr.length-1;
	  System.out.println(Arrays.toString(arr));
          qsort(arr,start,end);
	   System.out.println(Arrays.toString(arr));    
       }
}
