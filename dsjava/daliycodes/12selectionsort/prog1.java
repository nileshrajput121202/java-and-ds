import java.util.*;
class InsertionSort{
	static void isort(int arr[]){
            for(int i=0;i<arr.length;i++){
                int minindex=i;
		for(int j=i+1;j<arr.length;j++){
                   if(arr[j]<arr[minindex]){
                       minindex=j;
		   }
		}
		int temp=arr[i];
		arr[i]=arr[minindex];
		arr[minindex]=temp;
	    }
	}
       public static void main(String[] args){
            int arr[]=new int[]{6,5,72,5,7,5,3,6};
	    System.out.println(Arrays.toString(arr));
	    isort(arr);
	    System.out.println(Arrays.toString(arr));            
       }
}
