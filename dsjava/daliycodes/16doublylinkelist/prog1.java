import java.util.*;

class Node {
    int data;
    Node next = null;
    Node prev=null;
    Node(int data) {
        this.data = data;
    }
}

class DoublyLinkedList {
    Node head = null;

    void addfirst(int data) {
        Node newnode = new Node(data);
        if (head == null) {
            head = newnode;
        } else {
            newnode.next=head;
	    head.prev=newnode;
	    head=newnode;
        }
    }

    void addlast(int data) {
        Node newnode = new Node(data);
        if (head == null) {
            head = newnode;
        } else {
            Node temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = newnode;
	    newnode.prev=temp;

        }
    }

    int countnode() {
        Node temp = head;
        int cnt = 0;
        while (temp != null) {
            cnt++;
            temp = temp.next;
        }
        System.out.println("Node count: " + cnt);
        return cnt;
    }

    void addatpos(int pos, int data) {
        if (pos <= 0 || pos >= countnode() + 2) {
            System.out.println("wrong input");
            return;
        } else if (pos == 1) {
            addfirst(data);
        } else if (pos == countnode() + 1) {
            addlast(data);
        } else {
            Node newnode = new Node(data);
            Node temp = head;
            while (pos - 2 != 0) {
                temp = temp.next;
                pos--;
            }
            newnode.prev=temp;
	    newnode.next=temp.next;
	    temp.next=newnode;
	    newnode.next.prev=newnode;
        }
    }

    void deletefirst() {
        if (head == null) {
            System.out.println("empty linkedlist");
            return;
        }
        if (countnode() == 1) {
            head = null;
        } else {
            head = head.next;
	    head.prev=null;
        }
    }

    void deletelast() {
        if (head == null) {
            System.out.println("empty linkedlist");
            return;
        }
        if (countnode() == 1) {
            head = null;
        } else {
            Node temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.prev.next=null;
	    temp.prev=null;
        }
    }

    void deleteatpos(int pos) {
        if (pos <= 0 || pos >= countnode() + 1) {
            System.out.println("wrong input");
            return;
        }
        if (pos == 1) {
            deletefirst();
        } else if (pos == countnode()) {
            deletelast();
        } else {
            Node temp = head;
            while (pos - 2 != 0) {
                temp = temp.next;
                pos--;
            }
            temp.next = temp.next.next;
	    temp.next.prev=temp;
        }
    }

    void printll() {
        if (head == null) {
            System.out.println("empty linked list");
        } else {
            Node temp = head;
            while (temp != null) {
                System.out.print(temp.data + " ");
                temp = temp.next;
            }
            System.out.println();
        }
    }
}
class Client {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char ch;
        DoublyLinkedList dll = new DoublyLinkedList();

        do {
            System.out.println("Linked List");
            System.out.println("1. addfirst");
            System.out.println("2. addlast");
            System.out.println("3. addatposition");
            System.out.println("4. deletefirst");
            System.out.println("5. deletelast");
            System.out.println("6. deleteatposition");
            System.out.println("7. countnode");
            System.out.println("8. printlinkedlist");
            System.out.println("enter your choice...!");
            int choice = sc.nextInt();

       
	    switch (choice) {
                case 1: {
                    System.out.println("enter the data");
                    int data = sc.nextInt();
                    dll.addfirst(data);
                }
                    break;
                case 2: {
                    System.out.println("enter the data");
                    int data = sc.nextInt();
                    dll.addlast(data);
                }
                    break;
                case 3: {
                    System.out.println("enter the data");
                    int data = sc.nextInt();
                    System.out.println("enter the position");
                    int pos = sc.nextInt();
                    dll.addatpos(pos, data);
                }
                    break;

                case 4: {
                    dll.deletefirst();
                }
                    break;

                case 5: {
                    dll.deletelast();
                }
                    break;

                case 6: {
                    System.out.println("enter the position");
                    int pos = sc.nextInt();
                    dll.deleteatpos(pos);
                }
                    break;

                case 7: {
                    dll.countnode();
                }
                    break;
                case 8: {
                    dll.printll();
                }
                    break;
                default:
                    System.out.println("wrong input");
                    break;
            }

            System.out.println("Do you want to continue (y/n)?");
            ch = sc.next().charAt(0);
        } while (ch == 'y' || ch == 'Y');
    }
}
