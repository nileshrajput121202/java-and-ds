// two stack using array
import java.util.*;
class TwoStack{
     int size;
     int stack[];
     int top1;
     int top2;
     TwoStack(int size){
        this.size=size;
	this.stack=new int[size];
	this.top1=-1;
	this.top2=size;
     }

     void push1(int data){
        if(top2-top1>1){
            top1++;                                                                                                                stack[top1]=data; 
	}else{
           System.out.println("stack 1 overflow");
	}
     }
            
     int pop1(){
         if(top1==-1){
	    System.out.println("stack 1 empty");
	    return -1;
	 }else{
              int val=stack[top1];
	      top1--;     
	      return val;
	 }
     }

     void push2(int data){
            if(top2-top1>1){
                top2--;
		stack[top2]=data;
	    }else{
                System.out.println("1 overflow");
	    }
     }

     int pop2(){
           if(top2==size){
              System.out.println("stack 2 empty");
	      return -1;
	   }else{
              int val= stack[top2];
	      top2++;
	      return val;
	   }
     }

     void print(){
        for(int i=0;i<=top1;i++){
           System.out.print(stack[i]);
	}
     }
}
class Client{
      public static void main(String [] args){
	   Scanner sc =new Scanner(System.in);
	   System.out.println("enter the size");
	   int size=sc.nextInt();
           TwoStack s=new TwoStack(size);
	   char ch;
	   do{
               System.out.println("1.push1\n2.pop1\n3.push2\n4.pop2\n5.print");
	       System.out.println("enter choice");
	       int c=sc.nextInt();

	       switch(c){
                 case 1:
			 {
                           System.out.println("enter data");
			   int data=sc.nextInt();
			   s.push1(data);   
			 }
			 break;
		 case 2:
			 {
                           System.out.println("stach 1 pop ele is:"+s.pop1());
			 }
			 break;
		 case 3:
			 {                                                                                                                       System.out.println("enter data");                                                                                     int data=sc.nextInt();                                                                                                s.push2(data);                                                                                                      }  
		       break;
	         case 4:
		       {
			   System.out.println("stach 2 pop ele is:"+s.pop2());    
		       }	       
		       break;
		 case 5:
		       {
                           s.print();
		       }
		       break;
		 default:
		       System.out.println("wrong input");
		       break;
		       
	       }
	       System.out.println("Do you want to continue");
	       ch=sc.next().charAt(0);
	   }while(ch=='y' || ch=='Y');
	   
      }
}
