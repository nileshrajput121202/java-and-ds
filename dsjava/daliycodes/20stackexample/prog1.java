// reverse string using stack
import java.util.*;
class RevStr{
        String rev(String str){
             char arr[]=new char[str.length()];
	     Stack<Character> s= new Stack<Character>();
	     for(int i=0;i<str.length();i++){
                  s.push(str.charAt(i));
	     }

	     int i=0;
	     while(!s.empty()){
                arr[i]=s.pop();
		i++;
	     }
	     return new String(arr);
	}
}
class Client{
     public static void main(String[] args){
         Scanner sc=new Scanner(System.in);
	 System.out.println("enter the string");
	 String str=sc.next();
	 RevStr rs =new RevStr();
	 String string= rs.rev(str);
	 System.out.println(string);
     }
}
