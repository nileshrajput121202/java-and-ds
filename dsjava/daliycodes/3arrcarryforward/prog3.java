/*  given a integer array of size N
    Build an array leftmax of size N
    rightmax of i contain the max for index 0 to i
    arr:[-3,6,2,4,5,2,8,-9,3,1]
    N=10
   
 */
class Demo {
    public static void main(String[] args) {
        int arr[] = new int[]{-3, 6, 2, 4, 5, 2, 8, -9, 3, 1};
        int N = 10;
        int rightmax[] = new int[N];
        rightmax[N-1]=arr[N-1];

	for(int i=N-2;i>0;i--){
           if(rightmax[i+1]<arr[i]){
                rightmax[i]=arr[i];
	   }
	   else{
               rightmax[i]=rightmax[i+1];
	   }
	}

	for(int i=0;i<N;i++){
           System.out.println(rightmax[i]);
	}
}
}
