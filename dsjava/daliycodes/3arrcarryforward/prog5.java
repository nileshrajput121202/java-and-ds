/*  given an character array (lowercase) return the count of pair(i,j)such that 
 *  i<j
 *  arr[i]='a';
 *  arr[j]='g';
 *  arr:[a,b,e,g,a,g]
 *  output:3
 */

class Demo{
    public static void main(String[] args){
         char arr[]={'a','b','e','g','a','g'};
	 int cnt=0;
	 int pair=0;
	 for(int i=0;i<arr.length;i++){
               if(arr[i]=='a'){
                   cnt++;
	       }
	       else if(arr[i]=='g'){
                  pair=pair+cnt;
	       }
	 }
	 System.out.println(pair);

    }
}
