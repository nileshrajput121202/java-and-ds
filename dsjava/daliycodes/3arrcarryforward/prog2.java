/*  given a integer array of size N
    Build an array leftmax of size N
    leftmax of i contain the max for index 0 to i
    arr:[-3,6,2,4,5,2,8,-9,3,1]
    N=10
    leftmax:[-3,6,6,6,6,6,8,8,8,8]
 */
class Demo {
    public static void main(String[] args) {
        int arr[] = new int[]{-3, 6, 2, 4, 5, 2, 8, -9, 3, 1};
        int N = 10;
        int leftmax[] = new int[N];
        leftmax[0]=arr[0];

	for(int i=1;i<arr.length;i++){
           if(leftmax[i-1]<arr[i]){
                leftmax[i]=arr[i];
	   }
	   else{
               leftmax[i]=leftmax[i-1];
	   }
	}

	for(int i=0;i<N;i++){
           System.out.println(leftmax[i]);
	}
}
}
