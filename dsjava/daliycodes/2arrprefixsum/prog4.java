/*
  given an array of size N and Q number of quries, quries contain two parameters(s,e)
  s=>start index and e=>end index
  for all quries print the sum of all elements from indexs to index e
  arr:[-3,6,2,4,5,2,8,-9,3,1]
  N=10;
  Q=3;
  Quries    s    e     sum
  Query1    1    3     12
  Query2    2    7     12
  Query3    1    1     6
  1<=N<=10^5
  1<=A[i]<=10^3

*/
//using prefix sum
//optimize code
import java.util.*;
class Demo{
      public static void main(String[] args){
           int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};
	   int N=10;
	   int Q=3;
           Scanner sc=new Scanner(System.in);
	   
	   System.out.println("normal array:");
	   for(int i=0;i<N;i++){
		  System.out.println(arr[i]);
	      }

	   for(int i=1;i<N;i++){
              arr[i]=arr[i-1]+arr[i];
	   }
           int sum=0;
	   System.out.println("prefix sum array:");
	   for(int i=0;i<N;i++){
		  System.out.println(arr[i]);
	      }
	   }

	   }
      
