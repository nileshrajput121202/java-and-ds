// sum of array elements from start to end index
import java.util.*;
class Demo{
     public static void main(String[] args){
           Scanner sc=new Scanner(System.in);
	   System.out.println("enter the start and end index");
	   int s=sc.nextInt();
	   int e=sc.nextInt();

	   int arr[]=new int[]{1,2,3,4,5,6,7,8,9,10};
           int sum=0;
	   
	   for(int i=s;i<=e;i++){
                sum=sum+arr[i];
	   }
	   System.out.println(sum);
	   
     }
}
