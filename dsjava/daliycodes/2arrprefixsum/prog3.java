/*
  given an array of size N and Q number of quries, quries contain two parameters(s,e)
  s=>start index and e=>end index
  for all quries print the sum of all elements from indexs to index e
  arr:[-3,6,2,4,5,2,8,-9,3,1]
  N=10;
  Q=3;
  Quries    s    e     sum
  Query1    1    3     12
  Query2    2    7     12
  Query3    1    1     6
*/
//using prefix sum
//optimize code
import java.util.*;
class Demo{
      public static void main(String[] args){
           int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};
	   int N=10;
	   int Q=3;
           Scanner sc=new Scanner(System.in);
	   int psarr[]=new int[10];
           psarr[0]=arr[0];
	   for(int i=1;i<N;i++){
              psarr[i]=psarr[i-1]+arr[i];
	   }
           int sum=0;
	   for(int i=0;i<Q;i++){
		    int s=sc.nextInt();                                                                                                   int e=sc.nextInt();   
	      if(s==0){
                System.out.println(psarr[e]);
	      }
	      else{
              
	      sum=psarr[e]-psarr[s-1];
	      System.out.println(sum);
	      }
	   }

	   }
      }
