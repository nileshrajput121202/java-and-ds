import java.util.*;

class Demo {
    static int fun(int arr[], int search) {
        int start = 0;
        int end = arr.length - 1;
        while (start <= end) {
            int mid = (start + end) / 2;
            if (arr[mid] == search) {
                return mid;
            }
            if (arr[mid] > search) {
                end = mid - 1;
            }
            if (arr[mid] < search) {
                start = mid + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Array Size");
        int size = sc.nextInt();
        System.out.println("Enter Array Elements");
        int arr[] = new int[size];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }
        System.out.println();

        System.out.println("Array elements are:");

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        System.out.println("Enter Element for Search");
        int search = sc.nextInt();
        int ret = fun(arr, search);

        if (ret == -1) {
            System.out.println("Element not found in the array");
        } else {
            System.out.println("Element found at index: " + ret);
        }
    }
}
