import java.util.*;

class Demo {
    static int fun(int arr[], int search,int start,int end) {
       
            if(start>end){
               return -1;
	    }	    
            int mid = (start + end) / 2;

            if (arr[mid] == search) {
                return mid;
            }
            else if (arr[mid] > search) {
                end = mid - 1;
            }
	    else {
                start = mid + 1;
            }

	    return fun( arr, search, start, end);
        }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Array Size");
        int size = sc.nextInt();
        System.out.println("Enter Array Elements");
        int arr[] = new int[size];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }
        System.out.println();

        System.out.println("Array elements are:");

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        System.out.println("Enter Element for Search");
        int search = sc.nextInt();
	 int start = 0;                                                                                                        int end = arr.length - 1;     
        int ret = fun(arr, search,start,end);

        if (ret == -1) {
            System.out.println("Element not found in the array");
        } else {
            System.out.println("Element found at index: " + ret);
        }
    }
}
