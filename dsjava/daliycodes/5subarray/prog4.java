//print sum of each subarray,using prefix sum
class SubarrayDemo{
      public static void main(String[] args){
            int arr[]=new int[]{1,2,3};
	    int psarr[]=new int[arr.length];
	    psarr[0]=arr[0];
            for(int i=1;i<arr.length;i++){
                psarr[i]=psarr[i-1]+arr[i];
	    }
            int sum=0;
	    for(int i=0;i<arr.length;i++){
                for(int j=i;j<arr.length;j++){
                   if(i==0){
                     sum=psarr[j];
		   }else{
                      sum=psarr[j]-psarr[i-1];
		   }
		   System.out.println(sum);
		}
	    }
	    
      }
}
