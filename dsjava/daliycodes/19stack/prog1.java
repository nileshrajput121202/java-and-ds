import java.util.*;
class Stack{
      int maxsize;
      int stack[];
      Stack(int size){
         this.maxsize=size;
	 this.stack=new int[size];
      }
      int top=-1;
      void push(int data){
        if(top==maxsize-1){
           System.out.println("stack overflow");
	}else{
           top++;
	   stack[top]=data;
	}
      }

      boolean empty(){
           if(top==-1){
               return true;
	   }else{
               return false;
	   }
      }

      int pop(){
          if(empty()){
              System.out.println("stack is empty");
	      return -1;
	  }else{
             int val=stack[top];
	     top--;
	     return val;
	  }
      }

      int peek(){
          if(empty()){
              System.out.println("stack is empty");                                                                                 return -1;    
	  }else{
             int val=stack[top];
	     return val;
	  }
      }

      int size(){
          return top;
      }

      void printstack(){
            if(empty()){
                System.out.println("stack is empty");                                                                                 return ;
	    }else{
	      System.out.print("[ ");
              for(int i=0;i<=top;i++){
                 System.out.print(stack[i]+" ");
	      }
	       System.out.print(" ]");
               System.out.println();           	       
	    }
      }
}

class Client{
      public static void main(String[] args){
	 Scanner sc=new Scanner(System.in);
	 System.out.println("enter the size");
	 int size=sc.nextInt();
         Stack s=new Stack(size);
	 char ch;
	 do{
          System.out.println("1.push\n2.pop\n3.peek\n4.isempty\n5.size\n6.printstack");
	  System.out.println("enter your choice");
	  int c=sc.nextInt();
	  switch(c){
             case 1:
		     {
                       System.out.println("enter data");
		       int data=sc.nextInt();
		       s.push(data);
		     }
		     break;
	     case 2:
		     {
                       int val=s.pop();
		       System.out.println("pop element:"+val);
		     }
		     break;
	     case 3:
		     {
                      int val=s.peek();
		      System.out.println("top element:"+val);
		     }
		     break;
	     case 4:
		     {
                      boolean val=s.empty();
		      System.out.println(val);
		     }
		     break;
	     case 5:
		     {
                      System.out.println(s.size());
		     }
		     break;
	     case 6:
		     {
                      s.printstack();
		     }
		     break;
	     default:
		     System.out.println("wrong input");
		     break;
	  }
	  System.out.println("do you wanr to continue");
	  ch=sc.next().charAt(0);
	 }while(ch=='y'||ch=='Y');
      }
}
