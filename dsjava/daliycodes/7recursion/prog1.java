import java.util.*;
class Demo{
	static int fact(int n){
              if(n==0 || n==1){
                 return 1;
	      }
	      return n*fact(n-1);
	}
        public static void main(String[] args){
            Scanner sc =new Scanner(System.in);
	    System.out.println("enter the number");
	    int x=sc.nextInt();
            int ret = fact(x);
	    System.out.println(ret);
	}
}
