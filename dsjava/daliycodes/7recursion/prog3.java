import java.util.*;
class Demo{
	static String StrRev(String str){
              if(str==null || str.length()<=1){
                   return str;
	      }
	      return StrRev(str.substring(1))+str.charAt(0);
	}
        public static void main(String[] args){
            Scanner sc =new Scanner(System.in);
	    System.out.println("enter the number");
	    String str=sc.next();
            String ret = StrRev(str);
	    System.out.println(ret);
	}
}
